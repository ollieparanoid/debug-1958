#!/bin/sh -e

# DISABLE logic below,
exec /sbin/apk.orig "$@"


# We're interested in the second run
marker=/tmp/abuild-apk-wrapper-ran-once

if [ -n "$ABUILD_APK_WRAPPER" ]; then
	if [ -e $marker ]; then
		echo "(waiting for gdb)" >&2
		/usr/bin/qemu-aarch64-static -g 1337 /sbin/apk.orig "$@"
	else
		touch $marker
		/sbin/apk.orig "$@"
	fi
else
	/sbin/apk.orig "$@"
fi
