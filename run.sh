#!/bin/sh -ex
alias pmbootstrap=./pmbootstrap/pmbootstrap.py
WORK=~/.local/var/pmbootstrap

if [ "$USER" = "build" ]; then
	# assume builds.sr.ht
	sudo apk -q add tmux neovim tree xz

	git config --global user.email e
	git config --global user.name u
fi

# Patch pmbootstrap
if ! [ -e /tmp/patched ]; then
	cd pmbootstrap
	git am ../debug-1958/patches/pmb/0001*
	touch /tmp/patched
	cd ..
fi

# Reset chroots
pmbootstrap -yq zap -p
pmbootstrap -q chroot -- true
pmbootstrap -q chroot --add=file,lua-dev -baarch64 -- true

# Build abuild from source, to narrow down the bug with additional logging
rm -rf "pmaports/temp/apk-tools"
mkdir -p "pmaports/temp/apk-tools"
cp debug-1958/aports/apk-tools/APKBUILD "pmaports/temp/apk-tools/APKBUILD"
pmbootstrap \
	--details-to-stdout \
	build \
	--arch=aarch64 \
	--src=$PWD/debug-1958/src/apk-tools-2.10.5 \
	apk-tools
pmbootstrap -q chroot -baarch64 -- sh -c 'apk add ~pmos/packages/pmos/aarch64/apk-tools-2.10.5_p*.apk'
pmbootstrap -q chroot -baarch64 -- file /sbin/apk

# Install wrapper for abuild-apk
# /usr/local/bin/abuild-apk -> /usr/bin/abuild-apk
LOCALBIN="$WORK/chroot_buildroot_aarch64/usr/local/bin"
mkdir -p "$LOCALBIN"
sudo cp debug-1958/wrappers/abuild-apk.sh "$LOCALBIN/abuild-apk"

# Install wrapper for /sbin/apk (called by suid-binary /usr/bin/abuild-apk)
# /sbin/apk -> /sbin/apk.orig
SBIN="$WORK/chroot_buildroot_aarch64/sbin"
sudo mv "$SBIN/apk" "$SBIN/apk.orig"
sudo cp debug-1958/wrappers/apk.sh "$SBIN/apk"

# Mount aarch64 chroot in native chroot
sudo mkdir -p "$WORK/chroot_native/aarch64"
sudo mount --bind "$WORK/chroot_buildroot_aarch64" "$WORK/chroot_native/aarch64"

# Reproduce the bug
pmbootstrap \
        -mp https://build.postmarketos.org/wip/ \
        -mp http://mirror.postmarketos.org/postmarketos/ \
        --details-to-stdout \
        \
        build \
        --arch=aarch64 \
        --force \
        --strict \
        phosh

echo "DONE"
