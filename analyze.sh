#!/bin/sh -ex
# This script should run locally where pmbootstrap is installed, not on
# builds.sr.ht.
WORK=~/.local/var/pmbootstrap
CORE=coredumps/core1.xz

# Reset chroots
pmbootstrap -yq zap
pmbootstrap -q chroot --add=file -- true
pmbootstrap -q chroot --add=file -baarch64 -- true

# Install binaries with debug symbols
sudo cp -R pkgs/x86_64 "$WORK/chroot_native/_pkgs"
pmbootstrap -q chroot -- apk --allow-untrusted -q add /_pkgs/qemu-aarch64-5.0.0-r1.apk
pmbootstrap -q chroot -- file /usr/bin/qemu-aarch64

sudo cp -R pkgs/aarch64 "$WORK/chroot_buildroot_aarch64/_pkgs"
pmbootstrap -q chroot -baarch64 -- apk --allow-untrusted -q add /_pkgs/apk-tools-2.10.5-r1.apk
pmbootstrap -q chroot -baarch64 -- file /sbin/apk

# Copy and extract corefile
sudo cp "$CORE" "$WORK/chroot_native/core.xz"
pmbootstrap -q chroot --add=xz -- unxz /core.xz

# Copy dump-guest-memory.py
sudo cp dump-guest-memory.py "$WORK/chroot_native/dump.py"

# Load corefile into gdb
pmbootstrap chroot --add=gdb,python3 -- gdb /usr/bin/qemu-aarch64 /core

# I assumed that we could get the guest memory now, with:
# (gdb) source /dump.py
# (gdb) dump-guest-memory /gcore aarch64-le
# but if fails:
# Python Exception <class 'gdb.error'> No symbol "address_space_memory" in current context.: 
# Error occurred in Python: No symbol "address_space_memory" in current context.

