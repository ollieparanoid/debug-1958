#!/bin/sh -ex
alias pmbootstrap=./pmbootstrap/pmbootstrap.py
WORK=~/.local/var/pmbootstrap

sudo apk -q add tmux neovim tree xz

git config --global user.email e
git config --global user.name u

# Enable coredumps
ulimit -c unlimited

# Patch pmbootstrap
if ! [ -e /tmp/patched ]; then
	cd pmbootstrap
	git am ../debug-1958/patches/pmb/0001*
	touch /tmp/patched
	cd ..
fi

# Reset chroots
pmbootstrap -yq zap
pmbootstrap -q chroot --add=file -- true
pmbootstrap -q chroot --add=file -baarch64 -- true

# Install binaries with debug symbols
sudo cp -R debug-1958/pkgs/x86_64 "$WORK/chroot_native/_pkgs"
pmbootstrap -q chroot -- apk --allow-untrusted add /_pkgs/qemu-aarch64-5.0.0-r1.apk
pmbootstrap -q chroot -- file /usr/bin/qemu-aarch64

sudo cp -R debug-1958/pkgs/aarch64 "$WORK/chroot_buildroot_aarch64/_pkgs"
pmbootstrap -q chroot -baarch64 -- apk --allow-untrusted add /_pkgs/apk-tools-2.10.5-r1.apk
pmbootstrap -q chroot -baarch64 -- file /sbin/apk

# Reproduce the bug
pmbootstrap \
        -mp https://build.postmarketos.org/wip/ \
        -mp http://mirror.postmarketos.org/postmarketos/ \
        --details-to-stdout \
        \
        build \
        --arch=aarch64 \
        --force \
        --strict \
        phosh || true

# Copy and compress core
CORE="$WORK/chroot_buildroot_aarch64/home/pmos/build/core"
sudo cp "$CORE" .
sudo chown build:build core
ls -lah core
rm -f core.xz
xz -T0 core
ls -lah core.xz

# Thru the wormhole
sudo apk -q add \
	py3-attrs \
	py3-automat \
	py3-click \
	py3-pip \
	py3-pynacl \
	py3-six \
	py3-tqdm \
	py3-twisted
sudo pip3 install magic-wormhole
wormhole send core.xz

echo "DONE"
