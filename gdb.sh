#!/bin/sh -ex
# Run in second terminal after run.sh was started, to attach gdb
alias pmbootstrap=./pmbootstrap/pmbootstrap.py
pmbootstrap -q chroot -- \
	gdb-multiarch -q \
		-ex 'set architecture aarch64' \
		-ex 'file /aarch64/sbin/apk.orig' \
		-ex 'set sysroot /aarch64' \
		-ex 'target remote localhost:1337'

#		-ex 'continue' \
#		-ex 'bt'
